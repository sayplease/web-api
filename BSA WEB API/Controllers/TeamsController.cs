﻿using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TeamsController : ControllerBase
{
    private readonly TeamService _teamService;

    public TeamsController(TeamService teamService)
    {
        _teamService = teamService;
    }

    [HttpGet]
    public List<TeamDTO> GetAllTeams()
    {
        return _teamService.GetAllTeams();
    }
    
    [HttpGet("{id:int}")]
    public TeamDTO GetById(int id)
    {
        return _teamService.GetById(id);
    }
}