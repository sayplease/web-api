﻿using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class TasksController : ControllerBase
{
   private readonly TaskService _taskService;

   public TasksController(TaskService taskService)
   {
       _taskService = taskService;
   }
   
   [HttpGet]
   public List<TaskDTO> GetAllTasks()
   {
       return _taskService.GetAllTasks();
   }

   [HttpGet("{id:int}")]
   public TaskDTO GetById(int id)
   {
       return _taskService.GetById(id);
   }
}