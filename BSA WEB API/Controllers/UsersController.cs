﻿using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private readonly UserService _userService;

    public UsersController(UserService userService)
    {
        _userService = userService;
    }
    
    [HttpGet]
    public List<UserDTO> GetAllUsers()
    {
        return _userService.GetAllUsers();
    }
    
    [HttpGet("{id:int}")]
    public UserDTO GetById(int id)
    {
        return _userService.GetById(id);
    }

}