﻿



using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;




namespace BSA_WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }
        
        [HttpGet]  
        public  List<ProjectDTO> GetAll()
        {
            return  _projectService.GetAllProject();
        }

        [HttpGet("{id:int}")]
        public ProjectDTO GetProjById(int id)
        {
            return  _projectService.GetById(id);
        }
    }
}
