﻿using System.Reflection;
using API.BLL.MapingProfile;
using API.BLL.Services;


namespace BSA_WEB_API.Extensions;

public static class ServiceExtensions
{
    public static void RegisterAutoMapper(this IServiceCollection services)
    {
        services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
    }

    public static void RegisterCustomServices(this IServiceCollection services)
    {
        services.AddScoped<ProjectService>();
        services.AddScoped<TaskService>();
        services.AddScoped<TeamService>();
        services.AddScoped<UserService>();
    }
}