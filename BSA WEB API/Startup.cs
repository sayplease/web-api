﻿


using API.DAL.Entities;
using API.DAL.Repository;
using BSA_WEB_API.Extensions;

namespace BSA_WEB_API
{
    public class Startup
    {
    
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.RegisterCustomServices();
            services.RegisterAutoMapper();
            services.AddTransient<IRepository<Project>, ProjectRepository>();
            services.AddTransient<IRepository<API.DAL.Entities.Task>, TaskRepository>();
            services.AddTransient<IRepository<Team>, TeamRepository>();
            services.AddTransient<IRepository<User>, UserRepository>();
            
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
           
            app.UseEndpoints(cfg =>
            {
                cfg.MapControllers();
            });
        }
    }
}
