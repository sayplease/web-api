﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Repository;
using AutoMapper;
using Task = API.DAL.Entities.Task;

namespace API.BLL.Services;

public class TaskService : BaseService
{
    private readonly IRepository<Task> _repository;
    public TaskService(IMapper mapper, IRepository<Task> repository) : base(mapper)
    {
        _repository = repository;
    }

    public TaskDTO GetById(int id)
    {
        var task = _repository.GetById(id);
        return _mapper.Map<TaskDTO>(task);
    }

    public List<TaskDTO> GetAllTasks()
    {
        var tasks = _repository.GetAll();
        return _mapper.Map<List<TaskDTO>>(tasks);
    }
}