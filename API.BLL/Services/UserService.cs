﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Entities;
using API.DAL.Repository;
using AutoMapper;

namespace API.BLL.Services;

public class UserService : BaseService
{
    private readonly IRepository<User> _repository;

    public UserService(IMapper mapper, IRepository<User> repository) : base(mapper)
    {
        _repository = repository;
    }

    public List<UserDTO> GetAllUsers()
    {
        var users = _repository.GetAll();
        
        return _mapper.Map<List<UserDTO>>(users);
    }

    public UserDTO GetById(int id)
    {
        var user = _repository.GetById(id);

        return _mapper.Map<UserDTO>(user);
    }
}