﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Entities;
using API.DAL.Repository;
using AutoMapper;




namespace API.BLL.Services;
 
public sealed class ProjectService : BaseService
{
    private readonly IRepository<Project> _repository;

    public ProjectService(IMapper mapper, IRepository<Project> repository ) : base(mapper)
    {
        _repository = repository;
    }

    public  ProjectDTO GetById(int id)
    {
        var proj =  _repository.GetById(id);
        return  _mapper.Map<ProjectDTO>(proj);
    }

    public List<ProjectDTO> GetAllProject()
    {
        var projects = _repository.GetAll();
        return  _mapper.Map<List<ProjectDTO>>(projects);
    }
}