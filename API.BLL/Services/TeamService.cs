﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Entities;
using API.DAL.Repository;
using AutoMapper;

namespace API.BLL.Services;

public class TeamService : BaseService
{
    private readonly IRepository<Team> _repository;
    public TeamService(IMapper mapper, IRepository<Team> repository) : base(mapper)
    {
        _repository = repository;
    }

    public List<TeamDTO> GetAllTeams()
    {
        var teams = _repository.GetAll();
        return _mapper.Map<List<TeamDTO>>(teams);
    }

    public TeamDTO GetById(int id)
    {
        var team = _repository.GetById(id);
        return _mapper.Map<TeamDTO>(team);
    }
}