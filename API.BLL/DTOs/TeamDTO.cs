﻿namespace API.BLL.DTOs;

public sealed class TeamDTO
{
    public int Id { get; set; }


    public string Name { get; set; }


    public DateTime? CreatedAt { get; set; }
}