﻿namespace API.DAL.Entities
{
    public class Team
    {
        public int Id { get; set; }


        public string Name { get; set; }


        public DateTime? CreatedAt { get; set; }
    }
}
