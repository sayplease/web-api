﻿

using API.DAL.EnumState;

namespace API.DAL.Entities
{
    public class Task
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public TaskState? State { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }

        public Project Project { get; set; }

        public User User { get; set; }
 
    }

}
