﻿using API.DAL.Entities;

namespace API.DAL.Repository
{
    public interface IRepository<T> where T : class
    {
        List<T> GetAll();
        T GetById(int id);
        T Create(T entity);
        T Update(T entity);
        T Delete(int id);

    }
}
