﻿

using API.DAL.Entities;

namespace API.DAL.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private List<Project> _projectList;

       public ProjectRepository()
        {
            _projectList = new List<Project>()
            {
                new Project() { Id = 1, Name = "BinaryStudioAcademy", Description = "Learn and Teach",
                    CreatedAt = new DateTime(2021,01,17), AuthorId = 1, Deadline = new DateTime(2021,01,25), TeamId = 1,},
                new Project() { Id = 2, Name = "BinaryStudioAcademy2", Description = "Learn and Teach",
                    CreatedAt = new DateTime(2021,02,17), AuthorId = 2, Deadline = new DateTime(2021,02,25), TeamId = 2 },
                new Project() { Id = 3, Name = "BinaryStudioAcademy3", Description = "Learn and Teach",
                    CreatedAt = new DateTime(2021,01,17), AuthorId = 3, Deadline = new DateTime(2021,01,25), TeamId = 3 },
            };
        }

        public Project Create(Project entity)
        {
            entity.Id = _projectList.Max(e => e.Id) + 1;
            entity.TeamId = entity.Team.Id;
            entity.AuthorId = entity.User.Id;
            _projectList.Add(entity);
            return entity;
        }

        public Project Delete(int id)
        {
            Project project = _projectList.FirstOrDefault(e => e.Id == id);
            if(project != null)
            {
                _projectList.Remove(project); 
            }
            return project;
        }

        public  List<Project> GetAll()
        {
            return  _projectList;
        }

        public Project GetById(int id)
        {
            return _projectList.FirstOrDefault(e => e.Id == id);
        }

        public Project Update(Project entity)
        {
            Project project = _projectList.FirstOrDefault(e => e.Id == entity.Id);
            if (project != null)
            {
                project.Name = entity.Name;
                project.Description = entity.Description;
                project.CreatedAt = entity.CreatedAt;
                project.TeamId = entity.TeamId;
                project.Deadline = entity.Deadline;
                project.AuthorId = entity.AuthorId;
            }
            return project;
        }
    }
}
