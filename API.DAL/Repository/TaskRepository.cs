﻿

namespace API.DAL.Repository
{
    public class TaskRepository : IRepository<Entities.Task>
    {
        private List<Entities.Task> _tasks;

        public TaskRepository()
        {
            _tasks = new List<Entities.Task>()
            {
                new Entities.Task() { Id = 1, Name = "Repository pattern and LINQ",
                    CreatedAt = DateTime.Now, Description = "DataBase", PerformerId = 1, State = EnumState.TaskState.Create, ProjectId = 1 },
                new Entities.Task() { Id = 2, Name = "Repository pattern and LINQ2",
                    CreatedAt = DateTime.Now, Description = "DataBase", PerformerId = 2, State = EnumState.TaskState.ToDo, ProjectId = 2 },
                new Entities.Task() { Id = 2, Name = "Repository pattern and LINQ3",
                    CreatedAt = DateTime.Now, Description = "DataBase", PerformerId = 3, State = EnumState.TaskState.ToDo, ProjectId = 3 },
            };

        }

        Entities.Task IRepository<Entities.Task>.Create(Entities.Task entity)
        {
            entity.Id = _tasks.Max(e => e.Id) + 1;
            _tasks.Add(entity);
            return entity;
        }

        Entities.Task IRepository<Entities.Task>.Delete(int id)
        {
            Entities.Task task = _tasks.FirstOrDefault(e => e.Id == id);
            if (task != null)
            {
                _tasks.Remove(task);
            }
            return task;
        }

        List<Entities.Task> IRepository<Entities.Task>.GetAll()
        {
            return _tasks;
        }

        Entities.Task IRepository<Entities.Task>.GetById(int id)
        {
            return _tasks.FirstOrDefault(e => e.Id == id);
        }

        Entities.Task IRepository<Entities.Task>.Update(Entities.Task entity)
        {
            Entities.Task task = _tasks.FirstOrDefault(e => e.Id == entity.Id);
            if (task != null)
            {
                task.Id = entity.Id;
                task.Name = entity.Name;
                task.CreatedAt = entity.CreatedAt;
                task.FinishedAt = entity.FinishedAt;
                task.Description = entity.Description;
                task.PerformerId = entity.PerformerId;
                task.State = entity.State;
            }
            return task;
        }
    }
}
