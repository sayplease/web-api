﻿using API.DAL.Entities;


namespace API.DAL.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private List<Team> _teams;

        public TeamRepository()
        {
            _teams = new List<Team>()
            {
                new Team() { Id = 1, Name = "SOAD", CreatedAt = DateTime.Now },
                new Team() { Id = 2, Name = "Rammstein", CreatedAt = DateTime.Now },
                new Team() { Id = 3, Name = "Deep Purple", CreatedAt = DateTime.Now },
            } ;
        }
        public Team Create(Team entity)
        {
            entity.Id = _teams.Max(e => e.Id) +1;
            _teams.Add(entity);
            return entity;

        }

        public Team Delete(int id)
        {
            Team team = _teams.FirstOrDefault(e => e.Id == id);
            if (team != null)
            {
                _teams.Remove(team);
            }
            return team;
        }

        public List<Team> GetAll()
        {
            return _teams;
        }

        public Team GetById(int id)
        {
            return _teams.FirstOrDefault(e => e.Id == id);
        }

        public Team Update(Team entity)
        {
            Team team = _teams.FirstOrDefault(e => e.Id == entity.Id);
            if (team != null)
            {
                team.Id = entity.Id;
                team.Name = entity.Name;
                team.CreatedAt = entity.CreatedAt;
            }
            return team;
        }
    }
}
