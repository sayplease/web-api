﻿using API.DAL.Entities;

namespace API.DAL.Repository
{
    public class UserRepository : IRepository<User>
    {
        private List<User> _users;

        public UserRepository()
        {
            _users = new List<User>()
            {
                new User()
                {
                    Id = 1, Email = "programme@ukr.net", FirstName = "Bohdan", LastName = "Patternovich",
                    RegisteredAt = new DateTime(2021, 03, 13), TeamId = 10,
                    BirthDay = new DateTime(1993, 03, 02),
                    Team = new Team()
                    {
                        Id = 10, Name = "SuperMario", CreatedAt = new DateTime(2021, 03, 13),
                    }
                },
                new User()
                {
                    Id = 2, Email = "programme1@ukr.net", FirstName = "Bohdan1", LastName = "Patternovich1",
                    RegisteredAt = new DateTime(2021, 03, 13), TeamId = 11,
                    BirthDay = new DateTime(1993, 03, 02),
                    Team = new Team()
                    {
                        Id = 11, Name = "SuperMario", CreatedAt = new DateTime(2021, 03, 13),
                    }
                },
                new User()
                {
                    Id = 3, Email = "programme2@ukr.net", FirstName = "Bohdan2", LastName = "Patternovich2",
                    RegisteredAt = new DateTime(2021, 03, 13), TeamId = 12,
                    BirthDay = new DateTime(1993, 03, 02),
                    Team = new Team()
                    {
                        Id = 12, Name = "SuperMario", CreatedAt = new DateTime(2021, 03, 13),
                    }
                }
                
            };
        }
        public List<User> GetAll()
        {
            return _users;
        }

        public User GetById(int id)
        {
            return _users.FirstOrDefault(e => e.Id == id);
        }

        public User Create(User entity)
        {
            entity.Id = _users.Max(e => e.Id) +1;
            _users.Add(entity);
            return entity;
        }

        public User Update(User entity)
        {
            User user = _users.FirstOrDefault(e => e.Id == entity.Id);
            if (user != null)
            {
                user.Id = entity.Id;
                user.Email = entity.Email;
                user.Team = entity.Team;
                user.BirthDay = entity.BirthDay;
                user.FirstName = entity.FirstName;
                user.LastName = entity.LastName;
                user.RegisteredAt = entity.RegisteredAt;
                user.TeamId = entity.TeamId;
            }
            return user;
        }

        public User Delete(int id)
        {
            User user = _users.FirstOrDefault(e => e.Id == id);
            if (user != null)
            {
                _users.Remove(user);
            }
            return user;
        }
    }
}
