﻿namespace API.DAL.EnumState
{
    public enum TaskState
    {
        Create = 0,

        ToDo = 1,

        Testing = 2,

        Done = 3,

    }
}
